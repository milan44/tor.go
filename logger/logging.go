package logger

import (
	"github.com/gookit/color"
	"time"
)

func Log(log string) {
	gray := color.New(color.FgGray)
	_printLogTime("LOG")
	gray.Println(log)
}
func Warning(log string) {
	yellow := color.New(color.FgYellow)
	_printLogTime("WRN")
	yellow.Println(log)
}
func Custom(log string, logType string) {
	green := color.New(color.FgGreen)
	_printLogTime(logType)
	green.Println(log)
}


func _printLogTime(logType string) {
	gray := color.New(color.FgGray)

	t := time.Now()
	gray.Print(t.Format("[01/02/2006 15:04:05 ") + logType + "] ")
}