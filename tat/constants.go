package tat

import "regexp"

const NOT_AVAILABLE = "No, but thank you"
const SUCCESS = "Thank You"
const FAILED = "Uhm, no"
const HELLO = "Hello"
const DATA_TRANSFER = "This is for you "
const UNKNOWN_SESSION = "I don't know you"
const INVALID_CERTIFICATE = "Sorry, but i don't trust you"
const SEPERATOR = "\n99|99\n"

const KEY_RESPONSE = "response"

func TrimDataTransfer(str string) string {
	reg := regexp.MustCompile("^" + DATA_TRANSFER)
	return reg.ReplaceAllString(str, "")
}
