package json

import (
	"encoding/json"
	"net/url"
)

type JSONArray struct {
	Map map[string]string
}

func New(data map[string]string) JSONArray {
	if data != nil {
		return JSONArray{Map: data}
	}
	return JSONArray{Map: make(map[string]string)}
}
func ParseString(js string) (JSONArray, error) {
	jsonArray := New(nil)
	err := json.Unmarshal([]byte(js), &jsonArray.Map)
	if err != nil {
		return JSONArray{}, err
	}
	return jsonArray, nil
}
func IsValidJSON(js string) bool {
	_, err := ParseString(js)
	return err == nil
}
func PrepareURLValues(data url.Values) (url.Values, error) {
	jsonArray := New(nil)

	for key, value := range data {
		jsonArray.Set(key, value[0])
	}

	js, err := jsonArray.AsJSON()
	if err != nil {
		return nil, err
	}

	return url.Values{
		"s": {js},
	}, nil
}

func (j *JSONArray) Set(key string, value string) {
	j.Map[key] = value
}
func (j *JSONArray) Has(key string) bool {
	if _, ok := j.Map[key]; ok {
		return true
	}
	return false
}
func (j *JSONArray) Get(key string) string {
	if j.Has(key) {
		return j.Map[key]
	}
	return ""
}

func (j *JSONArray) AsJSON() (string, error) {
	js, err := json.Marshal(j.Map)
	if err != nil {
		return "", err
	}
	return string(js), nil
}
