package cert

import (
	"errors"
	"github.com/skip2/go-qrcode"
	"gitlab.com/milan44/tor.go/crypto"
	"gitlab.com/milan44/tor.go/files"
	"net/url"
	"strings"
)

type Certificater struct {
	SourceFolder string
}

func (c Certificater) IsValid(uri string, publicKey string) (bool, error) {
	if strings.HasPrefix(uri, "http://") || strings.HasPrefix(uri, "https://") {
		u, err := url.Parse(uri)
		if err != nil {
			return false, err
		}

		uri = u.Host
	}

	publicKey = strings.TrimSpace(publicKey)

	hashedPublicKey := crypto.SHA512Hash(publicKey)

	_ = files.CreateDirectory(c.SourceFolder)

	fileList := files.ListFilesInDirectory(c.SourceFolder)

	if fileList == nil {
		return false, errors.New("failed to list files in directory")
	}

	for _, file := range fileList {
		certificates, err := files.ReadFileAsArray(file)
		if err != nil {
			return false, err
		}

		for _, certificate := range certificates {
			certificate = strings.TrimSpace(certificate)
			array := strings.Split(certificate, " ")
			if strings.HasPrefix(certificate, "#") || len(array) != 2 {
				continue
			}
			if array[0] == uri && array[1] == hashedPublicKey {
				return true, nil
			}
		}
	}

	return false, nil
}
func (c Certificater) QRCode(certificate string, filename string) error {
	err := qrcode.WriteFile(certificate, qrcode.Medium, 512, filename)
	return err
}
func (c Certificater) CreateCertificate(uri string, publicKey string) (string, error) {
	if strings.HasPrefix(uri, "http://") || strings.HasPrefix(uri, "https://") {
		u, err := url.Parse(uri)
		if err != nil {
			return "", err
		}

		uri = u.Host
	}
	publicKey = strings.TrimSpace(publicKey)

	hashedPublicKey := crypto.SHA512Hash(publicKey)

	return uri + " " + hashedPublicKey, nil
}

func New(sourceFile string) Certificater {
	return Certificater{SourceFolder: sourceFile}
}
