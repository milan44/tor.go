package http

import (
	"gitlab.com/milan44/wintor"
	"io/ioutil"
	"net/url"
	"strings"
	"time"
)

func Post(uri string, data url.Values) (string, error) {
	client := wintor.GetClient(wintor.ClientConf{})
	client.Timeout = 60 * time.Second

	resp, err := client.PostForm(uri, data)
	if err != nil {
		return "", err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return strings.TrimSpace(string(body)), nil
}