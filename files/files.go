package files

import (
	"bufio"
	"fmt"
	"golang.org/x/crypto/ssh/terminal"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"syscall"
)

func ReadFile(filename string) (string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return "", err
	}
	defer file.Close()

	b, err := ioutil.ReadAll(file)
	if err != nil {
		return "", err
	}

	return strings.TrimSpace(string(b)), nil
}
func WriteFile(filename string, content string) error {
	data := []byte(content)
	err := ioutil.WriteFile(filename, data, 0777)
	if err != nil {
		return err
	}
	return nil
}
func FileExists(filename string) bool {
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		return false
	}
	return true
}
func ReadFileAsArray(filename string) ([]string, error) {
	contents, err := ReadFile(filename)
	if err != nil {
		return []string{}, err
	}

	return strings.Split(contents, "\n"), nil
}
func CreateDirectory(dirName string) bool {
	src, err := os.Stat(dirName)

	if os.IsNotExist(err) {
		errDir := os.MkdirAll(dirName, 0755)
		if errDir != nil {
			return false
		}
		return true
	}

	if src.Mode().IsRegular() {
		return false
	}

	return false
}
func ListFilesInDirectory(dirName string) []string {
	var files []string

	err := filepath.Walk(dirName, func(path string, info os.FileInfo, err error) error {
		if dirName == path {
			return nil
		}

		files = append(files, path)
		return nil
	})
	if err != nil {
		return nil
	}

	if len(files) == 0 {
		return nil
	}
	return files
}
func PasswordInput(prompt string) string {
	fmt.Print(prompt)
	bytePassword, err := terminal.ReadPassword(int(syscall.Stdin))
	if err != nil {
		return ""
	}
	password := string(bytePassword)

	return strings.TrimSpace(password)
}

func LoadOrCreatePersistentUserInput(filename string, prompt string) (string, error) {
	if FileExists(filename) {
		contents, err := ReadFile(filename)
		if err != nil {
			return "", err
		}

		if contents != "" {
			return contents, nil
		}
	}

	text := ReadNotEmptyValueFromUser(prompt)

	err := WriteFile(filename, text)
	if err != nil {
		return "", err
	}

	return text, nil
}
func ReadNotEmptyValueFromUser(prompt string) string {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print(prompt)
	text, _ := reader.ReadString('\n')

	for strings.TrimSpace(text) == "" {
		fmt.Println("Can't be empty!")
		fmt.Print(prompt)
		text, _ = reader.ReadString('\n')
	}

	return strings.TrimSpace(text)
}
