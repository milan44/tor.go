package session

import (
	"github.com/rs/xid"
	"gitlab.com/milan44/tor.go/ppk"
)

type SessionHandler struct {
	Sessions  map[string]Session
	MyKeyPair *ppk.KeyPair
}

func (s SessionHandler) SessionExists(sessionID string) bool {
	if _, ok := s.Sessions[sessionID]; ok {
		return true
	}
	return false
}
func (s SessionHandler) GetFreeSessionID() string {
	id := xid.New().String()
	for s.SessionExists(id) {
		id = xid.New().String()
	}
	return id
}
func (s *SessionHandler) KillSession(sessionID string) {
	if s.SessionExists(sessionID) {
		delete(s.Sessions, sessionID)
	}
}
func (s *SessionHandler) Update() {
	for id, session := range s.Sessions {
		if session.IsTooOld() {
			s.KillSession(id)
		}
	}
}
func (s *SessionHandler) CreateNewSession(remotePublicKey string) (Session, error) {
	session, err := NewSession(remotePublicKey, s.MyKeyPair)
	if err != nil {
		return Session{}, err
	}

	session.SessionID = s.GetFreeSessionID()

	s.Sessions[session.SessionID] = session

	return session, nil
}
func (s SessionHandler) GetSession(sessionID string) *Session {
	if s.SessionExists(sessionID) {
		session := s.Sessions[sessionID]
		return &session
	}
	return nil
}
func (s *SessionHandler) ModifySession(sessionID string) {
	if s.SessionExists(sessionID) {
		s.Sessions[sessionID].Modify()
	}
}
func (s *SessionHandler) UpdateSessionKey(sessionID string, key string) {
	if s.SessionExists(sessionID) {
		sess := s.Sessions[sessionID]
		sess.LastTrustedKey = key
		s.Sessions[sessionID] = sess
	}
}
func (s *SessionHandler) TrustSession(sessionID string, username string) {
	if s.SessionExists(sessionID) {
		sess := s.Sessions[sessionID]
		sess.IsTrusted = true
		sess.Username = username
		sess.Modify()
		s.Sessions[sessionID] = sess
	}
}

func NewSessionHandler(myKeyPair *ppk.KeyPair) SessionHandler {
	return SessionHandler{
		MyKeyPair: myKeyPair,
		Sessions:  make(map[string]Session),
	}
}
