package session

import (
	"crypto/rsa"
	"gitlab.com/milan44/tor.go/ppk"
	"time"
)

type Session struct {
	MyKeyPair       ppk.KeyPair
	RemotePublicKey rsa.PublicKey
	LastModifyTime  time.Time
	SessionID       string
	IsTrusted       bool
	Username        string
	LastTrustedKey  string
}

func (s Session) Encrypt(str string) (string, error) {
	return ppk.Encrypt(str, s.RemotePublicKey)
}
func (s Session) Decrypt(str string) (string, error) {
	return s.MyKeyPair.Decrypt(str)
}
func (s Session) Modify() {
	s.LastModifyTime = time.Now()
}
func (s Session) IsTooOld() bool {
	return time.Now().Sub(s.LastModifyTime) > 5*60*time.Second
}

func NewSession(remotePublicKey string, myKeyPair *ppk.KeyPair) (Session, error) {
	publicKey, err := ppk.GetPublicKeyFromString(remotePublicKey)
	if err != nil {
		return Session{}, err
	}

	session := Session{
		RemotePublicKey: publicKey,
		LastModifyTime:  time.Now(),
	}

	if myKeyPair == nil {
		myNewKeyPair, err := ppk.GenerateNewKeyPair()
		if err != nil {
			return Session{}, err
		}

		myKeyPair = &myNewKeyPair
	}

	session.MyKeyPair = *myKeyPair
	session.IsTrusted = false

	return session, nil
}
