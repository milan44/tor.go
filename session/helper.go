package session

import "github.com/rs/xid"

func _getNewSessionID() string {
	return xid.New().String()
}