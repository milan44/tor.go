package ppk

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/hex"
	"encoding/pem"
	"errors"
	"strings"
)

func _privateKeyFromString(privPEM string) (rsa.PrivateKey, error) {
	block, _ := pem.Decode([]byte(privPEM))
	if block == nil {
		return rsa.PrivateKey{}, errors.New("failed to decode block")
	}

	priv, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		return rsa.PrivateKey{}, err
	}

	return *priv, nil
}

func _splitForEncryption(text string) []string {
	byteArray := []byte(text)

	lastArray := make([]byte, 0)
	chunks := make([]string, 0)

	for x, byt := range byteArray {
		lastArray = append(lastArray, byt)

		if (x+1)%100 == 0 { // multiple of 100
			chunks = append(chunks, hex.EncodeToString(lastArray))
			lastArray = make([]byte, 0)
		}
	}
	if len(lastArray) > 0 {
		chunks = append(chunks, hex.EncodeToString(lastArray))
	}

	if len(chunks) == 0 {
		return []string{""}
	}
	return chunks
}
func _splitForDecryption(cyphertext string) ([][]byte, error) {
	result := make([][]byte, 0)
	strs := strings.Split(cyphertext, CHUNK_MARKER)

	for _, str := range strs {
		unhex, err := hex.DecodeString(str)
		if err != nil {
			return nil, err
		}
		result = append(result, unhex)
	}
	return result, nil
}
