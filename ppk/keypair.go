package ppk

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha512"
	"crypto/x509"
	"encoding/hex"
	"encoding/pem"
	"gitlab.com/milan44/tor.go/crypto"
	"gitlab.com/milan44/tor.go/files"
	"strings"
)

type KeyPair struct {
	PrivateKey rsa.PrivateKey
	PublicKey  rsa.PublicKey
}

func (k KeyPair) Decrypt(encryptedText string) (string, error) {
	encryptedText = strings.TrimSpace(encryptedText)
	chunks, err := _splitForDecryption(encryptedText)
	if err != nil {
		return "", err
	}
	result := make([]byte, 0)

	for _, chunk := range chunks {
		hash := sha512.New()
		plaintext, err := rsa.DecryptOAEP(hash, rand.Reader, &k.PrivateKey, chunk, nil)
		if err != nil {
			return "", err
		}
		hexEncoded := string(plaintext)

		byt, err := hex.DecodeString(hexEncoded)
		if err != nil {
			return "", err
		}

		result = append(result, byt...)
	}

	if len(result) == 0 {
		return "", nil
	}

	return string(result), nil
}
func (k KeyPair) SaveToFile(filename string, password string) error {
	privateKey := k.EncodedPrivateKey()
	encodedPrivateKey, err := crypto.AESEncrypt(password, privateKey)
	if err != nil {
		return err
	}
	err = files.WriteFile(filename, encodedPrivateKey)
	return err
}
func (k KeyPair) EncodedPrivateKey() string {
	privkey_bytes := x509.MarshalPKCS1PrivateKey(&k.PrivateKey)
	privkey_pem := pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: privkey_bytes,
		},
	)
	return string(privkey_pem)
}
func (k KeyPair) EncodedPublicKey() (string, error) {
	pubkey_bytes, err := x509.MarshalPKIXPublicKey(&k.PublicKey)
	if err != nil {
		return "", err
	}
	pubkey_pem := pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PUBLIC KEY",
			Bytes: pubkey_bytes,
		},
	)

	pub := string(pubkey_pem)

	return pub, nil
}
