package ppk

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha512"
	"crypto/x509"
	"encoding/hex"
	"encoding/pem"
	"errors"
	"gitlab.com/milan44/tor.go/crypto"
	"gitlab.com/milan44/tor.go/files"
	"strings"
)

const DECRYPTION_ERROR = "failed to decrypt"
const FILE_ERROR = "failed to read file"
const DECODING_ERROR = "failed to decode key"

const CHUNK_MARKER = "|-|-CHUNK-MARKER-|-|"

func GenerateNewKeyPair() (KeyPair, error) {
	privkey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		return KeyPair{}, err
	}

	keypair := KeyPair{
		PrivateKey: *privkey,
		PublicKey:  (*privkey).PublicKey,
	}

	return keypair, nil
}
func LoadKeyPairFromFile(filename string, password string) (KeyPair, error, string) {
	contents, err := files.ReadFile(filename)
	if err != nil {
		return KeyPair{}, err, FILE_ERROR
	}

	decodedPrivateKey, err := crypto.AESDecrypt(password, contents)

	if decodedPrivateKey == "" {
		return KeyPair{}, errors.New(DECRYPTION_ERROR), DECRYPTION_ERROR
	}

	if err != nil {
		return KeyPair{}, err, DECRYPTION_ERROR
	}

	privkey, err := _privateKeyFromString(decodedPrivateKey)
	if err != nil {
		return KeyPair{}, err, DECODING_ERROR
	}

	keypair := KeyPair{
		PrivateKey: privkey,
		PublicKey:  privkey.PublicKey,
	}

	return keypair, nil, ""
}
func LoadOrCreateKeyPair(filename string, password string) (KeyPair, error) {
	if files.FileExists(filename) {
		keyPair, err, errorType := LoadKeyPairFromFile(filename, password)

		if errorType != FILE_ERROR && err != nil {
			return KeyPair{}, err
		}

		if err == nil {
			return keyPair, nil
		}
	}

	keyPair, err := GenerateNewKeyPair()
	if err != nil {
		return KeyPair{}, nil
	}

	err = keyPair.SaveToFile(filename, password)
	if err != nil {
		return KeyPair{}, nil
	}

	return keyPair, nil
}
func Encrypt(text string, remotePublicKey rsa.PublicKey) (string, error) {
	hash := sha512.New()
	results := make([]string, 0)
	chunks := _splitForEncryption(text)

	for _, chunk := range chunks {
		ciphertext, err := rsa.EncryptOAEP(hash, rand.Reader, &remotePublicKey, []byte(chunk), nil)
		if err != nil {
			return "", err
		}

		results = append(results, hex.EncodeToString(ciphertext))
	}

	if len(results) == 0 {
		return "", nil
	}

	return strings.Join(results, CHUNK_MARKER), nil
}
func GetPublicKeyFromString(str string) (rsa.PublicKey, error) {
	block, _ := pem.Decode([]byte(str))
	if block == nil {
		return rsa.PublicKey{}, errors.New("failed to decode block")
	}

	pub, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return rsa.PublicKey{}, err
	}

	switch pub := pub.(type) {
	case *rsa.PublicKey:
		return *pub, nil
	default:
		break
	}
	return rsa.PublicKey{}, errors.New("unknown error occurred")
}
