package crypto

import (
	"crypto/sha512"
	"encoding/hex"
)

func SHA512Hash(str string) string {
	hasher := sha512.New()
	hasher.Write([]byte(str))
	return hex.EncodeToString(hasher.Sum(nil))
}